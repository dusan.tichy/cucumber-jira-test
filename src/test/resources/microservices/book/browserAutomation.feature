Feature: My browser automation feature
  Scenario: User enters incorrect guess and as a result, appropriate message is shown
     When user Joe navigates to the login page by opening Firefox
     Given he enters incorrect guess
     Then Text "Oops!" should appear on the page

 Scenario: User enters correct guess and as a result, appropriate message is shown
    When user Mary navigates to the login page by opening Firefox
    Given she enters correct guess
    Then Text "Congratulations! Your guess is correct" should appear on the page