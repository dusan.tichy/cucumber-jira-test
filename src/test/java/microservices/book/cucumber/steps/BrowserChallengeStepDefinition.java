
package microservices.book.cucumber.steps;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import microservices.book.cucumber.actors.Challenge;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserChallengeStepDefinition {

    private WebDriver driver;
    private Challenge challengeActor;

    @When("user {word} navigates to the login page by opening Firefox")
    public void navigatesToBrowser(final String user) {
        System.setProperty("webdriver.gecko.driver", "D:\\projects\\cucumber-tests\\geckodriver.exe");
//        System.setProperty("webdriver.chrome.driver", "D:\\projects\\cucumber-tests\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        System.setProperty("webdriver.chrome.verboseLogging", "true");
        System.setProperty("webdriver.chrome.whitelistedIps", "");

        final ChromeOptions options = new ChromeOptions().setHeadless(true);

//        this.driver = new FirefoxDriver();
        this.driver = new ChromeDriver(options);
        this.driver.get("http://frontend");
        final WebElement element = this.driver.findElement(By.name("user"));
        element.sendKeys(user);
        this.challengeActor = new Challenge(user);
    }

    @Given("she enters correct guess")
    public void findElementAndFillWithName() {
        final String correctGuess = this.driver.findElement(By.className("challenge")).getText();
        final String[] factors = correctGuess.split("x");
        final int result = Integer.parseInt(factors[0].trim()) * Integer.parseInt(factors[1].trim());
        final WebElement element = this.driver.findElement(By.name("guess"));
        element.sendKeys(result + "");
        element.submit();

    }
    @Given("he enters incorrect guess")
    public void findElementAndFill() {
        final String correctGuess = this.driver.findElement(By.className("challenge")).getText();
        final String[] factors = correctGuess.split("x");
        final int result = Integer.parseInt(factors[0].trim()) * Integer.parseInt(factors[1].trim()) -1;
        final WebElement element = this.driver.findElement(By.name("guess"));
        element.sendKeys(result + "");
        element.submit();

    }

    @Then("Text {string} should appear on the page")
    public void findIfUserAppearsInStatsTable(final String text) {
        //To wait for element visible

        final WebDriverWait wait = new WebDriverWait(this.driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4")));
        final WebElement textDemo = this.driver.findElement (By.xpath ("//*[contains(text(),'"+text+"')]"));
        Assert.assertTrue(textDemo.isDisplayed());
    }

    @After("@tagToIdentifyThatBeginAfterShouldRunForThisFeatureOnly")
    public void closeBrowser(final Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) this.driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", "name");
        }
        this.driver.quit();
    }


}
